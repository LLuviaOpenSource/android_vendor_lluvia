ifeq ($(LLUVIA_BUILD_TYPE), OFFICIAL)

LLUVIA_OTA_VERSION_CODE := ten

LLUVIA_PROPERTIES += \
    com.lluvia.ota.version_code=$(LLUVIA_OTA_VERSION_CODE) \
    sys.ota.disable_uncrypt=1

PRODUCT_PACKAGES += \
    Updates

PRODUCT_COPY_FILES += \
    vendor/lluvia/config/permissions/com.lluvia.ota.xml:system/etc/permissions/com.lluvia.ota.xml

endif
